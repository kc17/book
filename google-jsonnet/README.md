# jsonnet

jsonnet 是由 google 推出的一個 開源(Apache-2.0) 配置語言  
用於 增強和彌補 JSON 的 不足

官網 [http://jsonnet.org/](http://jsonnet.org/)  
github [https://github.com/google/jsonnet](https://github.com/google/jsonnet)