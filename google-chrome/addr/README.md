# 特殊地址
chrome 支持一些特殊地址 這些地址 作爲chrome 一些額外功能的入口

輸入 chrome://about 可以 查看到所有 支持的 地址

# 詳情
chrome://version/  
這個地址 顯示了 chrome 的一些詳情 包括 版本 os v8 用戶目錄 下載目錄 等

![chrome version](assets/chrome___version_.png)

# 實驗功能
chrome://flags/  
此地址 包含了 chrome 的一些 實驗性 功能 以供好奇者冒險 除非你真的知道自己在做什麼 否則不要 貿然修改這些設置

![chrome flags](assets/chrome___flags_.png)

# 設定
chrome://settings

如果你嫌按鈕太慢 直接輸入 這個地址就可以打開 設定

# 擴展
chrome://extensions  
此頁面可以管理 擴展功能 最主要的就是安裝的插件

# 網路
chrome://net-internals

此地址 可以查看 chrome 的網路詳情 包括 網路事件 dns http 代理 等

![chrome net internals](assets/chrome___net-internals_.png)


# 組件
chrome://components

此地址 可以管理 chrome 使用的 所有組件

![chrome components](assets/chrome___components_.png)

# 瀏覽歷史
chrome://history/
此地址可以查看到所有的 瀏覽歷史 如果同步了設備 還能看到其它設備中的瀏覽記錄

# 書籤管理
chrome://bookmarks

# 插件安裝管理
chrome://apps

# 下載記錄
chrome://downloads