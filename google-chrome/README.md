# chrome

chrome 是 Google 2008年開始 發布的 人類世界最好的\(沒有之一\) 網頁 瀏覽器

官網 [https://www.google.com/chrome/](https://www.google.com/chrome/)

最新離線 安裝包 [https://www.google.com/intl/zh-TW/chrome/browser/?standalone=1](https://www.google.com/intl/zh-TW/chrome/browser/?standalone=1)

# Chromium

Chromium 是 google 爲發展 Chrome 而開源\(BSD\)出來的的 一個 瀏覽器

Chromium 和 Chrome 基本是 同樣的 內核 架構 只是 Chromium 有更快的 版本更新 和最新 功能 而Chrome 會等到 這些功能穩定後才納入 Chromium 是 Chrome的 前哨實驗室

Chrome 作爲一個商業產品 爲了用戶體驗 會加入一些私有項目 而 這些項目 和功能 Chromium 可能不具備

官網 [https://www.chromium.org/](https://www.chromium.org/)