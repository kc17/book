# c++
* -lprotobuf
* -lprotobuf-lite
* -lprotoc

# Example
```sh
protoc -I pb/ pb/pb.proto --cpp_out=pb
```

**pb/pb.proto**
```text
syntax = "proto3";
package pb;
 
message Cat{
	repeated string name = 1;
	int64 id = 2;
	int32 lv = 3;
}
message Dog{
	int64 id = 1;
	string name = 2;
	string Food = 3;
}
```

**main.cpp**
```c++
#include "pb/pb.pb.h"
 
bool test_write(std::string& bCat,std::string& bDog)
{
    /***	cat	***/
    pb::Cat cat;
    cat.set_id(1);
    cat.add_name("cat");
    cat.add_name("cat");
    cat.set_name(1,"Cat");
    cat.set_lv(10);
 
    //cat.SerializeToOstream(&std::cout);
    //boost::scoped_array<char> data(new char[cat.ByteSizeLong()]);
    //cat.SerializeToArray(data.get(),cat.ByteSizeLong());
    if(!cat.SerializeToString(&bCat))
    {
        std::cout<<"bad write cat"<<std::endl;
        return false;
    }
 
    /***	dog	***/
    pb::Dog dog;
    dog.set_id(100);
    dog.set_name("Dog");
    dog.set_food("meat 蛋糕");
    if(!dog.SerializeToString(&bDog))
    {
        std::cout<<"bad write dog"<<std::endl;
        return false;
    }
 
    return true;
}
bool test_read(std::string& bCat,std::string& bDog)
{
    pb::Cat cat;
    pb::Dog dog;
    //cat.Clear();
    //dog.Clear();
    /***	cat	***/
    if(!cat.ParseFromString(bCat))
    {
        std::cout<<"bad read cat"<<std::endl;
        return false;
    }
    std::cout<<"id = "<<cat.id()<<"\nname = [";
    for(int i=0; i<cat.name_size(); ++i)
    {
        if(i)
        {
            std::cout<<",";
        }
        std::cout<<cat.name(i);
    }
    std::cout<<"]\nlv = "<<cat.lv()<<"\n";
 
    /***	dog	***/
    if(!dog.ParseFromString(bDog))
    {
        std::cout<<"bad read dog"<<std::endl;
        return false;
    }
    std::cout<<"id = "<<dog.id()
             <<"\nname = "<<dog.name()
             <<"\nfood = "<<dog.food()<<"\n";
    return true;
}
int main()
{
    //驗證 庫 是否和當前 兼容
    GOOGLE_PROTOBUF_VERIFY_VERSION;
 
    std::string bCat,bDog;
    test_write(bCat,bDog) && test_read(bCat,bDog);
 
    return 0;
}
```