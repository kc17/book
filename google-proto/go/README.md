# go
官方提供了 對 go 的支持

執行 `go get -u github.com/golang/protobuf/protoc-gen-go` 即可安裝  
protoc-gen-go 會被安裝 到 $GOPATH/bin 中  
protoc 需要 調用 protoc-gen-go 來生成 go代碼

# Example
```sh
protoc animal.proto --go_out=./animal
```

**animal.proto**
```text
syntax = "proto3";
package animal;
 
message Cat{
	repeated string name = 1;
	int64 id = 2;
	int32 lv = 3;
}
message Dog{
	int64 id = 1;
	string name = 2;
	string Food = 3;
}
```

**main.go**
```go
package main
 
import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"log"
	"test_protoc/animal"
)
 
func main() {
	// write
	cat := &animal.Cat{
		Id:   1,
		Name: []string{"cat", "Cat"},
		Lv:   10,
	}
	dog := &animal.Dog{
		Id:   100,
		Name: "Dog",
		Food: "meat 蛋糕",
	}
	var bCat, bDog []byte
	var e error
	bCat, e = proto.Marshal(cat)
	if e != nil {
		log.Fatalln(e)
	}
	bDog, e = proto.Marshal(dog)
	if e != nil {
		log.Fatalln(e)
	}
 
	// read
	cat.Reset()
	dog.Reset()
	if e = proto.Unmarshal(bCat, cat); e != nil {
		log.Fatalln(e)
	}
	if e = proto.Unmarshal(bDog, dog); e != nil {
		log.Fatalln(e)
	}
	fmt.Println(*cat)
	fmt.Println(*dog)
}
```