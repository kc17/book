# grpc

* 由 google 開發的 一個 開源\(Apache-2.0\) 高性能 RPC 框架
* 內部使用 http2 協議 通信 可以 高效的 復用 tcp
* 使用 protoc3 高效的 序列化數據
* 支持多種語言間的 服務器 客戶端 通信 \(**go c++** java python ruby c\# **node.js** android-java objective-c php\)

官網 [https://grpc.io/docs/](https://grpc.io/docs/)

源碼 [https://github.com/grpc](https://github.com/grpc)