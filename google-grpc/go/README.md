# 環境配置

```bash
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u google.golang.org/grpc
```

# TLS

grpc 可以很容易的支持 TLC  
只需要 在創建 服務 和 客戶端 時 傳入 TransportCredentials 相關配置 即可

google.golang.org/grpc/credentials 包提供來相關功能

```go
//創建 rpc 服務器
creds, e := credentials.NewServerTLSFromFile("server.pem", "server.key")
if e != nil {
	log.Fatalln(e)
}
s := grpc.NewServer(
	//設置 tls
	grpc.Creds(creds),
)
```

```go
//連接服務器
creds := credentials.NewTLS(&tls.Config{
	InsecureSkipVerify: true,
})
conn, e := grpc.Dial(
	Addr,
	grpc.WithTransportCredentials(creds),
)
```

# TransportCredentials 接口

只要 實現 TransportCredentials 接口 即可 實現自定義的 傳輸通道 驗證  
TransportCredentials 會在第一次建立 tcp 時 進行 驗證

```go
type TransportCredentials interface {
	// 客戶端 使用 此函數 向 服務器 進行 身份 認證
	//
	// 實現 應該 依據 context.Context 的上下問 進行 超時處理 以及 撤銷操作
	//
	// 如果 返回暫時性 錯誤 gRPC 會 嘗試 重新連接
	// (io.EOF, context.DeadlineExceeded or err.Temporary() == true).
	// 如果錯誤是臨時 應該 實現 Temporary() bool 因為 會進行 err.Temporary() == true 測試 如果不實現則認為不是臨時錯誤
 
	// 如果 返回 net.Conn 是 closed 則 實現應該 close 傳入的 net.Conn
	ClientHandshake(context.Context, string, net.Conn) (net.Conn, AuthInfo, error)
	//此函數 用於 服務器 對 客戶端的 認證處理
	//
	// 如果 返回 net.Conn 是 closed 則 實現應該 close 傳入的 net.Conn
	ServerHandshake(net.Conn) (net.Conn, AuthInfo, error)
	//此函數 用於 為人類 返回 協議的 一些文字描述
	Info() ProtocolInfo
	//此函數 用戶 創建一個 副本
	Clone() TransportCredentials
	//此函數 用於 覆蓋 證書 標明的 服務器 主機名稱
	OverrideServerName(string) error
}
```

## Example

```go
package transport
 
import (
	"encoding/binary"
	"errors"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc/credentials"
	"io"
	"log"
	"net"
)
 
type authInfo struct {
}
 
func (authInfo) AuthType() string {
	return "test transport auth"
}
 
type transportCredentials struct {
	isServer bool
	pwd      string
}
 
func NewServerCreds(pwd string) credentials.TransportCredentials {
	return &transportCredentials{
		isServer: true,
		pwd:      pwd,
	}
}
func NewClientCreds(pwd string) credentials.TransportCredentials {
	return &transportCredentials{
		isServer: false,
		pwd:      pwd,
	}
}
 
//實現 客戶端 向 服務器 請求認證
func (t *transportCredentials) ClientHandshake(ctx context.Context, str string, c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	fmt.Println("ClientHandshake")
 
	//向 服務器 發送 密碼
	b := make([]byte, 2+len(t.pwd))
	binary.LittleEndian.PutUint16(b, 2+uint16(len(t.pwd)))
	copy(b[2:], []byte(t.pwd))
	_, e := c.Write(b)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	}
	//獲取 服務器 確認
	b, e = t.getMessage(c)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	} else if len(b) != 1 || b[0] == 0 {
		e = errors.New("passwd not match")
		log.Println(e)
		return nil, nil, e
	}
	fmt.Println("c yes", b)
	//返回 net.Conn
	return c, authInfo{}, nil
}
 
//實現 服務器 驗證 客戶端
func (t *transportCredentials) ServerHandshake(c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	fmt.Println("ServerHandshake")
 
	//獲取 客戶端 發來的 驗證
	b, e := t.getMessage(c)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	}
 
	//驗證 密碼
	if string(b) != t.pwd {
		//通知 客戶端 失敗
		b = make([]byte, 3)
		binary.LittleEndian.PutUint16(b, 3)
		b[2] = 0
		_, e = c.Write(b)
		if e != nil {
			log.Println(e)
 
			return nil, nil, e
		}
		log.Println("pwd not match")
 
		return nil, nil, errors.New("pwd not match")
	}
 
	//通知 客戶端 成功
	b = make([]byte, 3)
	binary.LittleEndian.PutUint16(b, 3)
	b[2] = 1
	_, e = c.Write(b)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	}
	log.Println("s yes")
	//返回 net.Conn
	return c, authInfo{}, nil
}
 
func (t *transportCredentials) Info() credentials.ProtocolInfo {
	fmt.Println("Info")
	return credentials.ProtocolInfo{}
}
func (t *transportCredentials) Clone() credentials.TransportCredentials {
	fmt.Println("Clone")
	tn := *t
	return &tn
}
 
func (t *transportCredentials) OverrideServerName(string) error {
	fmt.Println("OverrideServerName")
	return nil
}
func (t *transportCredentials) getMessage(r io.Reader) (rs []byte, e error) {
	b := make([]byte, 1024)
	pos := 0
	var size int
	var n int
	for {
		//讀取 header
		n, e = r.Read(b[pos:])
		if e != nil {
			return
		}
		pos += n
		if pos < 2 {
			//wait header
			continue
		}
 
		if size == 0 {
			//獲取 長度
			size = int(binary.LittleEndian.Uint16(b))
			if size < 2 || size > 1024 {
				e = errors.New("bad msg len")
				return
			}
			b = b[:size]
		}
		if pos < size {
			//wait body
			continue
		}
		rs = b[2:]
		break
	}
	return
}
```

## Example TLS

```go
package transport
 
import (
	"encoding/binary"
	"errors"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc/credentials"
	"io"
	"log"
	"net"
)
 
type authInfo struct {
}
 
func (authInfo) AuthType() string {
	return "wrapper transport auth"
}
 
type transportCredentials struct {
	isServer bool
	pwd      string
	base     credentials.TransportCredentials
}
 
func NewServerCreds(pwd string, base credentials.TransportCredentials) credentials.TransportCredentials {
	return &transportCredentials{
		isServer: true,
		pwd:      pwd,
		base:     base,
	}
}
func NewClientCreds(pwd string, base credentials.TransportCredentials) credentials.TransportCredentials {
	return &transportCredentials{
		isServer: false,
		pwd:      pwd,
		base:     base,
	}
}
 
//實現 客戶端 向 服務器 請求認證
func (t *transportCredentials) ClientHandshake(ctx context.Context, str string, c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	fmt.Println("ClientHandshake")
	if t.base == nil {
		return t.clientHandshake(ctx, str, c)
	}
 
	//進行原始 驗證
	c0, i, e := t.base.ClientHandshake(ctx, str, c)
	if e != nil {
		return c0, i, e
	}
	return t.clientHandshake(ctx, str, c0)
}
func (t *transportCredentials) clientHandshake(ctx context.Context, str string, c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	//向 服務器 發送 密碼
	b := make([]byte, 2+len(t.pwd))
	binary.LittleEndian.PutUint16(b, 2+uint16(len(t.pwd)))
	copy(b[2:], []byte(t.pwd))
	_, e := c.Write(b)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	}
	//獲取 服務器 確認
	b, e = t.getMessage(c)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	} else if len(b) != 1 || b[0] == 0 {
		e = errors.New("passwd not match")
		log.Println(e)
		return nil, nil, e
	}
	fmt.Println("c yes", b)
	//返回 net.Conn
	return c, authInfo{}, nil
}
 
//實現 服務器 驗證 客戶端
func (t *transportCredentials) ServerHandshake(c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	fmt.Println("ServerHandshake", t.base)
	if t.base == nil {
		return t.serverHandshake(c)
	}
	//進行原始 驗證
	c0, i, e := t.base.ServerHandshake(c)
	if e != nil {
		return c0, i, e
	}
	return t.serverHandshake(c0)
}
func (t *transportCredentials) serverHandshake(c net.Conn) (net.Conn, credentials.AuthInfo, error) {
 
	//獲取 客戶端 發來的 驗證
	b, e := t.getMessage(c)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	}
 
	//驗證 密碼
	if string(b) != t.pwd {
		//通知 客戶端 失敗
		b = make([]byte, 3)
		binary.LittleEndian.PutUint16(b, 3)
		b[2] = 0
		_, e = c.Write(b)
		if e != nil {
			log.Println(e)
 
			return nil, nil, e
		}
		log.Println("pwd not match")
 
		return nil, nil, errors.New("pwd not match")
	}
 
	//通知 客戶端 成功
	b = make([]byte, 3)
	binary.LittleEndian.PutUint16(b, 3)
	b[2] = 1
	_, e = c.Write(b)
	if e != nil {
		log.Println(e)
 
		return nil, nil, e
	}
	log.Println("s yes")
	//返回 net.Conn
	return c, authInfo{}, nil
}
 
func (t *transportCredentials) Info() credentials.ProtocolInfo {
	fmt.Println("Info")
	return credentials.ProtocolInfo{}
}
func (t *transportCredentials) Clone() credentials.TransportCredentials {
	fmt.Println("Clone")
	tn := *t
	return &tn
}
 
func (t *transportCredentials) OverrideServerName(string) error {
	fmt.Println("OverrideServerName")
	return nil
}
func (t *transportCredentials) getMessage(r io.Reader) (rs []byte, e error) {
	b := make([]byte, 1024)
	pos := 0
	var size int
	var n int
	for {
		//讀取 header
		n, e = r.Read(b[pos:])
		if e != nil {
			return
		}
		pos += n
		if pos < 2 {
			//wait header
			continue
		}
 
		if size == 0 {
			//獲取 長度
			size = int(binary.LittleEndian.Uint16(b))
			if size < 2 || size > 1024 {
				e = errors.New("bad msg len")
				return
			}
			b = b[:size]
		}
		if pos < size {
			//wait body
			continue
		}
		rs = b[2:]
		break
	}
	return
}
```

# token

rpc 還支持使用 token  
token 會在 每次 請求時 都傳遞到 服務器 以便 在不同 請求時 可以 傳遞 不同的 token

## 實現 PerRPCCredentials 接口

```go
package basic
 
import (
	"golang.org/x/net/context"
	"google.golang.org/grpc/credentials"
)
 
// token 自定義 token 結構
type token struct {
	safe bool
}
 
// NewToken 返回 一個 token
func NewToken(safe bool) credentials.PerRPCCredentials {
	return token{safe: safe}
}
 
// RequireTransportSecurity 返回 是否 需要 安全傳輸 token
// 如果 返回 true 則必須 使用 WithTransportCredentials 創建 客戶端 (既安全的 加密通道)
func (t token) RequireTransportSecurity() bool {
	return t.safe
}
 
// GetRequestMetadata 返回 token 值
func (t token) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	//返回 token
	return map[string]string{
		"appid":  "101010",
		"appkey": "i am key",
	}, nil
}
```

## 客戶端 使用 WithPerRPCCredentials 創建 grpc 客戶端

```go
conn, e = grpc.Dial(
	Addr,
	grpc.WithTransportCredentials(creds),
	grpc.WithPerRPCCredentials(pb.NewToken()),
)
```

## 服務器 驗證 token

```go
package main
 
import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	pb "test/grpc/protoc"
)
 
const (
	LAddr = ":1102"
)
 
func main() {
	//創建 監聽 Listener
	l, e := net.Listen("tcp", LAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("work at", LAddr)
 
	//創建 rpc 服務器
	creds, e := credentials.NewServerTLSFromFile("test.crt", "test.key")
	if e != nil {
		log.Fatalln(e)
	}
	s := grpc.NewServer(
		//設置 tls
		grpc.Creds(creds),
	)
 
	//註冊 服務
	pb.RegisterTestServerServer(s, &testServer{})
 
	//註冊 反射 到 服務 路由
	reflection.Register(s)
 
	//讓 rpc 在 Listener 上 工作
	if e := s.Serve(l); e != nil {
		log.Fatalln(e)
	}
}
 
//定義 服務器
type testServer struct {
}
 
//實現 服務器 接口
func (s *testServer) Say(ctx context.Context, in *pb.SayRequest) (*pb.SayReply, error) {
	// 解析metada中的信息并验证
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "unknow token")
	}
	fmt.Println(md["appkey"])
 
	//邏輯 處理
	fmt.Printf("%v say : %v\n", in.Name, in.Text)
 
	//響應
	return &pb.SayReply{
			Text: "OK",
		},
		nil
}
```

即使沒有 設置 token  
metadata.FromIncomingContext\(ctx\) 通常 依然 會 返回 true 以為 grpc默認 會傳遞 幾個 自己的 token  
應該 在 FromIncomingContext 返回 true 之後 繼續 驗證 是否存在 自己 客戶端的 token


credentials.PerRPCCredentials 接口的 RequireTransportSecurity 在使用 WithTransportCredentials 時其實 沒有被調用  
所以 如果 不強制使用 WithTransportCredentials 可以直接在RequireTransportSecurity裡面 返回false  
則 是否 使用了 WithTransportCredentials 都可以正常 工作 \(但官方 沒有這麼說明 也不確保 後續代碼是否會變得 此 故一般不建議如此使用\)

# metadata

## google.golang.org/grpc/metadata

此包 提供了 對 token的 處理 方法

```go
	...
	//創建 token
	md := metadata.New(map[string]string{
		"appid":  "101010",
		"appkey": "i am key",
	})
	//將 token 手動設置到 傳出 環境
	ctx := context.Background()
	ctx = metadata.NewOutgoingContext(ctx, md)
	...
```

## UnaryInterceptor

使用 UnaryInterceptor 可以 為 rpc 服務設置 攔截器所有 請求 路由 前 都會調用 此 函數  
在此 驗證 token 並且 設置新的 token 是個 簡單 省事的 好主意

```go
	s := grpc.NewServer(
		//為服務 設置 攔截器
		grpc.UnaryInterceptor(
			func(ctx context.Context,
				req interface{},
				info *grpc.UnaryServerInfo,
				handler grpc.UnaryHandler) (resp interface{}, err error) {
 
				// 解析metada中的信息并验证
				md, ok := metadata.FromIncomingContext(ctx)
				if !ok {
					return nil, grpc.Errorf(codes.Unauthenticated, "unknow token")
				}
				if _, ok := md["appkey"]; !ok {
					return nil, grpc.Errorf(codes.Unauthenticated, "unknow token")
				}
 
				//設置 新的 token 值
				md = metadata.Pairs("lv", "10")
				ctx = metadata.NewIncomingContext(ctx, md)
				
				//路由
				return handler(ctx, req)
			},
		),
	)
```

Incoming 包含 傳遞過來的 token值 而 Outgoing 則時 傳出給遠端的 token值


token 的 key值 會被轉為小寫 並且只能是 ASCII

* digits: 0-9
* uppercase letters: A-Z \(normalized to lower\)
* lowercase letters: a-z
* special characters: -\_.

key 不用 是 以 **grpc-** 開始 這是 grpc 保留使用的

# build-proto.sh

build-proto.sh 是孤寫的一個 bash腳本 用於 自動 查找 檔案夾下 proto 定義並自動 調用 proto 編譯 grpc

```bash
#!/bin/bash
#Program:
#       自動編譯 google's Protocol Buffers 到 go 源碼
#History:
#       2018-03-09 king first release
#		2018-07-13 king fix BuildGRPC package
#Email:
#       zuiwuchang@gmail.com
 
# 創建 輸出檔案夾
function mkOutDir(){
	if test -d $1 ;then
		return
	fi
	
	mkdir -p $1
	ok=$?
	if [[ $ok != 0 ]];then
		exit $ok
	fi
}
# 查找 pb 檔案
function findPB(){
	_f_pb_ctx=`pwd`
	cd $1 && files=`find *.proto -type f` && cd $_f_pb_ctx
	ok=$?
	if [[ $ok != 0 ]];then
		exit $ok
	fi
}
# 編譯 pd
function BuildPB(){
	#mkOutDir $2
	
	findPB $1
	echo "protoc -I $1 --go_out=$2" $files
	protoc -I $1 --go_out=$2 $files
	ok=$?
	if [[ $ok != 0 ]];then
		exit $ok
	fi
}
 
# 編譯 grpc
# $1 $GOPATH 目錄
# $2 *.proto 所在目錄
# $3 package 目錄
function BuildGRPC(){
	findPB $2
	str=""
	for file in ${files[@]}
	do
		str="$str $3/$file"
	done
	
	echo "protoc -I $1 --go_out=plugins=grpc:$1" $str
	protoc -I $1 --go_out=plugins=grpc:$1 $str
	ok=$?
	if [[ $ok != 0 ]];then
		exit $ok
	fi
}
 
 
# 編譯 pb
BuildPB kcproto kcproto/go/kcproto
BuildPB kcproto/request kcproto/go/kcproto/request
 
# 編譯 grpc
# BuildGRPC 工作目錄 proto檔案目錄 輸出目錄
BuildGRPC ../ gservice kc-middle/gservice
BuildGRPC ../ gservice/wechant kc-middle/gservice/wechant
```

