# 基本玩法

## 創建 proto 檔案定義服務

創建 **test/net/basic/basic.proto** 檔案 在裡面 使用 protoc3 語法 描述 服務  
並且使用 protoc 創建 對應語言的 代碼 

```bash
protoc -I basic/ basic/basic.proto --go_out=plugins=grpc:basic
```

```text
syntax = "proto3";
 
package basic;
 
// 以 service 標記定義一個 服務
service TestServer {
	//以 rpc 定義 方法
	rpc Say(SayRequest) returns (SayReply) {}
}
 
/***	定義 proto3 數據 	***/
message SayRequest {
	string Name = 1;
	string Text = 2;
}
message SayReply{
	string	Text = 1;
}
```

## 服務器代碼

```go
package main
 
import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	pb "test/net/basic"
)
 
const (
	LAddr = ":1102"
)
 
func main() {
	//創建 監聽 Listener
	l, e := net.Listen("tcp", LAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("work at", LAddr)
 
	//創建 rpc 服務器
	s := grpc.NewServer()
 
	//註冊 服務
	pb.RegisterTestServerServer(s, &testServer{})
 
	//註冊 反射 到 服務 路由
	reflection.Register(s)
 
	//讓 rpc 在 Listener 上 工作
	if e := s.Serve(l); e != nil {
		log.Fatalln(e)
	}
}
 
//定義 服務器
type testServer struct {
}
 
//實現 服務器 接口
func (s *testServer) Say(ctx context.Context, in *pb.SayRequest) (*pb.SayReply, error) {
	//邏輯 處理
	fmt.Printf("%v say : %v\n", in.Name, in.Text)
 
	//響應
	return &pb.SayReply{
			Text: "OK",
		},
		nil
}
```

## 客戶端代碼

```go
package main
 
import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"strings"
	pb "test/net/basic"
)
 
const (
	Addr = "127.0.0.1:1102"
)
 
func main() {
	//連接 服務器
	conn, e := grpc.Dial(Addr, grpc.WithInsecure())
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	//創建 rpc 服務 客戶端
	c := pb.NewTestServerClient(conn)
	var cmd, name string
	for {
		fmt.Print("#>")
		fmt.Scan(&cmd)
		if cmd == "e" {
			break
		} else if strings.HasPrefix(cmd, "name=") {
			cmd = cmd[len("name="):]
			name = cmd
		} else if strings.HasPrefix(cmd, "say=") {
			cmd = cmd[len("say="):]
 
			if repl, e := c.Say(
				context.Background(),
				&pb.SayRequest{
					Name: name,
					Text: cmd,
				},
			); e == nil {
				fmt.Println(repl)
			} else {
				log.Println(e)
			}
		}
	}
}
```

* grpc 會自己維護 和服務器的 tcp 連接 使用 grpc 時 不需要使用者去 瞎操心 grpc.Dial 不會建立 tcp 而是 當 發送 rpc 請求時 才安需\(如果 已有可用連接 復用 否則 創建新連接\) 連接
* proto3中 定義的 rpc 方法 必須是 一個 傳入 參數 同 一個 返回值 且在服務器實現 和客戶端請求 時 這兩個值 不能為nil 除非服務 返回了錯誤
* 使用 service 來定義 一個 服務 grpc 使用 package 指定的 包名 和 service 指定的 服務名 來唯一確定一個 服務名稱 故 如果 兩個 服務 package 和 service 都指定來相同名稱 在 服務端在註冊 時將 出現錯誤 提示 服務被註冊到兩個實現中 package.service
* 同樣 grpc 使用來 package 指定的 包名 和 message 指定的 型別名 來唯一確定一個 型別 故 不要讓 package.message 同名
* 建於 以上 兩點 最合適的做法是 不要 出現同名的 package 你不應該有一個 web/data 包 和 game/data 包 這樣 package 就重名了 你應該使用類似 web.data 和 game.data 的包名

# stream

proto3 中的 rpc 方法 參數和 返回值 前 可以 加上 stream 以表示一個 流 故 可以 有 以下4種寫法

```text
rpc SayHello(HelloRequest) returns (HelloResponse){}
rpc LotsOfReplies(HelloRequest) returns (stream HelloResponse){}
rpc LotsOfGreetings(stream HelloRequest) returns (HelloResponse) {}
rpc BidiHello(stream HelloRequest) returns (stream HelloResponse){}
```

**\*.proto"**
```text
syntax = "proto3";
 
package basic;
 
// 以 service 標記定義一個 服務
service TestServer {
	//以 rpc 定義 方法
	rpc Say(SayRequest) returns (SayReply) {}
 
	//以 rpc 定義 方法
	rpc SayStream(stream SayRequest) returns (stream SayReply) {}
}
 
/***	定義 proto3 數據 	***/
message SayRequest {
	string Name = 1;
	string Text = 2;
}
message SayReply{
	string	Text = 1;
}
```

**server**
```go
package main
 
import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"io"
	"log"
	"net"
	pb "test/net/basic"
)
 
const (
	LAddr = ":1102"
)
 
func main() {
	//創建 監聽 Listener
	l, e := net.Listen("tcp", LAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("work at", LAddr)
 
	//創建 rpc 服務器
	s := grpc.NewServer()
 
	//註冊 服務
	pb.RegisterTestServerServer(s, &testServer{})
 
	//註冊 反射 到 服務 路由
	reflection.Register(s)
 
	//讓 rpc 在 Listener 上 工作
	if e := s.Serve(l); e != nil {
		log.Fatalln(e)
	}
}
 
//定義 服務器
type testServer struct {
}
 
//實現 服務器 接口
func (s *testServer) Say(ctx context.Context, in *pb.SayRequest) (*pb.SayReply, error) {
	//邏輯 處理
	fmt.Printf("%v say : %v\n", in.Name, in.Text)
 
	//響應
	return &pb.SayReply{
			Text: "OK",
		},
		nil
}
func (s *testServer) SayStream(stream pb.TestServer_SayStreamServer) (e error) {
	var req pb.SayRequest
	repl := &pb.SayReply{
		Text: "ok",
	}
	for {
		//獲取 請求
		e = stream.RecvMsg(&req)
		if e != nil {
			if e != io.EOF {
				log.Println(e)
			}
			return
		}
		fmt.Printf("%v say : %v\n", req.Name, req.Text)
 
		//響應
		e = stream.Send(repl)
		if e != nil {
			log.Println(e)
			return
		}
	}
	return
}
```

**client**
```go
package main
 
import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"strings"
	pb "test/net/basic"
	"time"
)
 
const (
	Addr = "127.0.0.1:1102"
)
 
func main() {
	//連接 服務器
	conn, e := grpc.Dial(Addr, grpc.WithInsecure())
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()
	//創建 rpc 服務 客戶端
	c := pb.NewTestServerClient(conn)
	var cmd, name string
	for {
		fmt.Print("#>")
		fmt.Scan(&cmd)
		if cmd == "e" {
			break
		} else if strings.HasPrefix(cmd, "name=") {
			cmd = cmd[len("name="):]
			name = cmd
		} else if strings.HasPrefix(cmd, "say=") {
			cmd = cmd[len("say="):]
 
			if repl, e := c.Say(
				context.Background(),
				&pb.SayRequest{
					Name: name,
					Text: cmd,
				},
			); e == nil {
				fmt.Println(repl)
			} else {
				log.Println(e)
			}
		} else if strings.HasPrefix(cmd, "say2=") {
			cmd = cmd[len("say2="):]
			stream, e := c.SayStream(context.Background())
			if e != nil {
				log.Println(e)
				break
			}
			var repl pb.SayReply
			for i := 0; i < 3; i++ {
				//發送
				stream.Send(&pb.SayRequest{
					Name: name,
					Text: cmd,
				})
 
				e = stream.RecvMsg(&repl)
				if e != nil {
					log.Println(e)
					break
				}
				fmt.Println(repl)
				time.Sleep(time.Second)
			}
			//通知 不會在有 請求到達
			stream.CloseSend()
		}
	}
}
```

# status

grpc 請求返回時 可以 包含一個 錯誤碼 和 錯誤描述  
google.golang.org/grpc/status 包 提供了 將 錯誤 包裝的 函數

```go
func Error(c codes.Code, msg string) error
func Errorf(c codes.Code, format string, a ...interface{}) error
```

## grpc.Code

google.golang.org/grpc 包的 Code 函數可以 返回 錯誤碼

```go
func Code(err error) codes.Code
```

## codes

google.golang.org/grpc/codes 包定義了 grpc 佔用了的 狀態碼

```go
// A Code is an unsigned 32-bit error code as defined in the gRPC spec.
type Code uint32
 
const (
	// OK is returned on success.
	OK Code = 0
 
	// Canceled indicates the operation was canceled (typically by the caller).
	Canceled Code = 1
 
	// Unknown error. An example of where this error may be returned is
	// if a Status value received from another address space belongs to
	// an error-space that is not known in this address space. Also
	// errors raised by APIs that do not return enough error information
	// may be converted to this error.
	// 未知錯誤 通常是由 API 返回的 錯誤 沒有足夠的信息將錯誤進行分類 
	Unknown Code = 2
 
	// InvalidArgument indicates client specified an invalid argument.
	// Note that this differs from FailedPrecondition. It indicates arguments
	// that are problematic regardless of the state of the system
	// (e.g., a malformed file name).
	// 調用者 傳入了 無效的 參數 系統無法 繼續執行此請求
	InvalidArgument Code = 3
 
	// DeadlineExceeded means operation expired before completion.
	// For operations that change the state of the system, this error may be
	// returned even if the operation has completed successfully. For
	// example, a successful response from a server could have been delayed
	// long enough for the deadline to expire.
	// 通常是 任務 超時 已經過期
	DeadlineExceeded Code = 4
 
	// NotFound means some requested entity (e.g., file or directory) was
	// not found.
	// 通常是 調用者請求的 資源 不存在
	NotFound Code = 5
 
	// AlreadyExists means an attempt to create an entity failed because one
	// already exists.
	// 通常是 調用者 請求創建的 資源 已經存在 所以無法 創建
	AlreadyExists Code = 6
 
	// PermissionDenied indicates the caller does not have permission to
	// execute the specified operation. It must not be used for rejections
	// caused by exhausting some resource (use ResourceExhausted
	// instead for those errors). It must not be
	// used if the caller cannot be identified (use Unauthenticated
	// instead for those errors).
	// 調用者 沒有權限
	PermissionDenied Code = 7
 
	// ResourceExhausted indicates some resource has been exhausted, perhaps
	// a per-user quota, or perhaps the entire file system is out of space.
	// 通常是 系統資源 或 分配給調用者的 資源已經耗盡 所以無法 執行 請求
	ResourceExhausted Code = 8
 
	// FailedPrecondition indicates operation was rejected because the
	// system is not in a state required for the operation's execution.
	// For example, directory to be deleted may be non-empty, an rmdir
	// operation is applied to a non-directory, etc.
	//
	// A litmus test that may help a service implementor in deciding
	// between FailedPrecondition, Aborted, and Unavailable:
	//  (a) Use Unavailable if the client can retry just the failing call.
	//  (b) Use Aborted if the client should retry at a higher-level
	//      (e.g., restarting a read-modify-write sequence).
	//  (c) Use FailedPrecondition if the client should not retry until
	//      the system state has been explicitly fixed. E.g., if an "rmdir"
	//      fails because the directory is non-empty, FailedPrecondition
	//      should be returned since the client should not retry unless
	//      they have first fixed up the directory by deleting files from it.
	//  (d) Use FailedPrecondition if the client performs conditional
	//      REST Get/Update/Delete on a resource and the resource on the
	//      server does not match the condition. E.g., conflicting
	//      read-modify-write on the same resource.
	// 通常 表示 錯誤 並且 客戶端 不應該 進行 重試操作 具體參數 如上  a b c d
	FailedPrecondition Code = 9
 
	// Aborted indicates the operation was aborted, typically due to a
	// concurrency issue like sequencer check failures, transaction aborts,
	// etc.
	//
	// See litmus test above for deciding between FailedPrecondition,
	// Aborted, and Unavailable.
	// 通常 類似 Unavailable  但如果要重試操作的化 應該使用 比 Unavailable 更高級別的 重試操作
	Aborted Code = 10
 
	// OutOfRange means operation was attempted past the valid range.
	// E.g., seeking or reading past end of file.
	//
	// Unlike InvalidArgument, this error indicates a problem that may
	// be fixed if the system state changes. For example, a 32-bit file
	// system will generate InvalidArgument if asked to read at an
	// offset that is not in the range [0,2^32-1], but it will generate
	// OutOfRange if asked to read from an offset past the current
	// file size.
	//
	// There is a fair bit of overlap between FailedPrecondition and
	// OutOfRange. We recommend using OutOfRange (the more specific
	// error) when it applies so that callers who are iterating through
	// a space can easily look for an OutOfRange error to detect when
	// they are done.
	// 通常表示 請求的數據已經越界 如操作數組期望的索引 讀取檔案尾之後的 數據
	OutOfRange Code = 11
 
	// Unimplemented indicates operation is not implemented or not
	// supported/enabled in this service.
	// 通常表示 一個 請求 系統 還未實現 可能會在將來實現
	Unimplemented Code = 12
 
	// Internal errors. Means some invariants expected by underlying
	// system has been broken. If you see one of these errors,
	// something is very broken.
	// 通常 這個錯誤 意味者 系統已經崩潰 但你不想讓用戶知道 所以還在讓程序 繼續假裝 運行 但整個系統 已經被破壞 無法如預期工作
	Internal Code = 13
 
	// Unavailable indicates the service is currently unavailable.
	// This is a most likely a transient condition and may be corrected
	// by retrying with a backoff.
	//
	// See litmus test above for deciding between FailedPrecondition,
	// Aborted, and Unavailable.
	//
	// 通常表示 當前服務不可用 但不可用的狀態是短暫的 客戶可以在之後進行 重試操作(例如 網路異常 tcp斷線都會返回 此錯誤)
	Unavailable Code = 14
 
	// DataLoss indicates unrecoverable data loss or corruption.
	// 通常表示 無法恢復數據 或 數據已經 損毀
	DataLoss Code = 15
 
	// Unauthenticated indicates the request does not have valid
	// authentication credentials for the operation.
	// 調用者的憑證 有問題 無法識別 調用者的 身份
	Unauthenticated Code = 16
)
```

如果 tcp 斷開了 通常會 返回 Unavailable 但 grpc 會自動重建 tcp  
所以你完全沒必要 重建 grpc 客戶端 或tcp 連接 只需要 重新發送失敗的請求進行 重試 會 繼續 請求其它功能即可

