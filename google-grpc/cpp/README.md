# c++

google 官方提供了 對c++的支持

* -lprotobuf  
* -lprotobuf-lite  
* -lprotoc  
* -lgrpc++

# ubuntu

1. 安裝 需要依賴的工具 `sudo apt-get install build-essential autoconf libtool`
2. 如果要運行 test 則需要安裝 如下 組件 `sudo apt-get install libgflags-dev libgtest-dev sudo apt-get install clang libc++-dev`
3. 下載 並且安裝 `git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc cd grpc git submodule update --init make sudo make install`

在 windows 下編譯前 需要 預定義宏 \_WIN32\_WINNT=0x600

# Example

## 創建 pb/pb.proto

protoc -I pb --cpp\_out=pb pb/pb.proto  
protoc -I pb --plugin=protoc-gen-grpc=which grpc\_cpp\_plugin --grpc\_out=pb pb/pb.proto 

> protoc-gen-grpc 後面是 插件的 全路徑 此處用 linux的 which 自動尋找  
> 如果在 sb的 windows 平臺下 只有手動 寫全路徑  
> protoc -I pb --plugin=protoc-gen-grpc=**c:/grp/grpc\_cpp\_plugin** --grpc\_out=pb pb/pb.proto
>
> **windows msys2**
>
> ```bash
> #!/bin/bash
>  
> export GrpcCppPlugin=`which grpc_cpp_plugin`.exe
>  
> protoc -I pb --cpp_out=pb pb/pb.proto
> protoc -I pb --plugin=protoc-gen-grpc=$GrpcCppPlugin --grpc_out=pb pb/pb.proto
> ```

```text
syntax = "proto3";
 
package pb;
 
// 以 service 標記定義一個 服務
service TestServer {
	//以 rpc 定義 方法
	rpc Say(SayRequest) returns (SayReply) {}
}
 
/***	定義 proto3 數據 	***/
message SayRequest {
	string Name = 1;
	string Text = 2;
}
message SayReply{
	string	Text = 1;
}
```

## server

**s/main.cpp**
```cpp
#include <iostream>
 
#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/server_credentials.h>
 
#include "pb/pb.grpc.pb.h"
 
#define LAddr "localhost:1102"
 
//定義 服務器
class TestServerImpl final : public pb::TestServer::Service
{
    //實現 服務器 接口
    grpc::Status Say(
        grpc::ServerContext* context,
        const pb::SayRequest* req,
        pb::SayReply* reply
    ) override
    {
        //邏輯 處理
        std::cout<<req->name()<<" say :"<<req->text()<<"\n";
 
        //響應
        reply->set_text(req->text());
 
        return grpc::Status::OK;
    }
};
 
int main()
{
    grpc::ServerBuilder builder;
    builder.AddListeningPort(
        LAddr,
        grpc::InsecureServerCredentials() // h2c
    );
 
    //實例化 服務器
    TestServerImpl service;
 
    //支持 服務
    builder.RegisterService(&service);
 
 
    //運行 服務
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << LAddr << std::endl;
 
    //等待服務 停止
    server->Wait();
 
    return 0;
}
```

## client

**c/main.cpp**
```cpp
#include <iostream>
#include <boost/algorithm/string.hpp>
 
#include <grpc/grpc.h>
#include <grpc++/channel.h>
#include <grpc++/client_context.h>
#include <grpc++/create_channel.h>
#include <grpc++/security/credentials.h>
 
#include "../pb/pb/pb.grpc.pb.h"
 
#define LAddr "localhost:1102"
 
int main()
{
    //創建 客戶端
    std::unique_ptr<pb::TestServer::Stub> stub = pb::TestServer::NewStub(
                grpc::CreateChannel(
                    LAddr,
                    grpc::InsecureChannelCredentials()
                )
            );
 
    std::string cmd;
    pb::SayRequest req;
    pb::SayReply repl;
    while(true)
    {
        std::cout<<"#>";
        std::cin>>cmd;
 
        if (cmd == "e")
        {
            break;
        }
        else if(boost::algorithm::starts_with(cmd, "name="))
        {
            req.set_name(cmd.substr(std::string("name=").size()));
        }
        else if(boost::algorithm::starts_with(cmd, "say="))
        {
            req.set_text(cmd.substr(std::string("say=").size()));
 
            //rpc 請求
            grpc::ClientContext     context;
            grpc::Status status = stub->Say(&context,req,&repl);
            if(!status.ok())
            {
                std::cout<<status.error_code()<<" "<<status.error_message()<<"\n";
                continue;
            }
            std::cout<<repl.text()<<"\n";
        }
    }
 
    return 0;
}
```

