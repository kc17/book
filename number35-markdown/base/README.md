# 哲學
markdown 的目標是 易讀易寫 但最強調的易讀 一份markdown的檔案 可以直接以純文字發佈 並且看起來不會 像是由 許多標籤或是格式指令 所構成 

markdown 全部語法由標點符號組成 並經過了嚴謹的慎選

# 行內 HTML
markdown 的目的不是取代HTML而是讓她更容易閱讀 HTML是一種發佈格式 markdown是一種編寫格式

markdown 的格式語法值覆蓋了文字可以覆蓋的範圍 不在 markdown 之外的標籤 可以直接在檔案中用 HTML書寫

區域元素 比如 &lt;div&gt; &lt;table&gt; &lt;pre&gt; &lt;p&gt; 等標籤 必須加上 空行 以便內容 區隔 並且不可在其首尾使用 tab會空格 縮排
```text
這是一個 <i>markdown</i> 段落

<table>
	<tr>
		<td>這是HTML區塊</td>
	</tr>
</table>

這是另外一個 <i>markdown</i> 段落
```

# 特殊字元自動轉換
在 HTML 中 < 和 & 用於標記 標籤的開始 和 HTML 實體

當書寫一個 實體時 比如 &lt; markdown 不會將 & 替換為 &amp; 但如果不是實體 比如 xxx@gmail.com 會自動 轉為 xxx&amp;gmail.com 以便html中是如你 預期的顯示

同樣的在書寫\<i\>標籤\</i\> 時 < 不會被轉換 而其它情況而會被 轉

在code範圍內 < & 無論如何都會被自動 轉換 以便你可以 把代碼 直接copy到區塊中
