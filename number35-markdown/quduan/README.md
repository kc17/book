# 連接
markdown 支持兩種形式的 連接語法 行內 和 參考

## 行內
使用 **\[xxx\]\(url "title"\)** 的形式 title是可選的
```text
This is [an example](http://example.com/ "Title") inline link.

[This link](http://example.net/) has no title attribute.
```

```HTML
<p>This is <a href="http://example.com/" title="Title">
an example</a> inline link.</p>

<p><a href="http://example.net/">This link</a> has no
title attribute.</p>
```

如果要訪問同主機的字元 可以使用 相對路徑
```text
See my [About](/about/) page for details.  
```

## 參考
使用 [id]: 在文檔任意位置定義一個 參考  
然後 將行內寫法的 (url) 替換爲 [id]

```text
[google]: https://www.google.com/  "Google"
[google]: https://www.google.com/  'Google'
[google]: https://www.google.com/  (Google)
```
```text
[google home][google]
[google][] 如果 標籤和參考一致可以省略 參考id
[google]: 上面連接的另外一種寫法
```
### 注意
連接id是不區分 大小寫的 所以 \[google home\]\[google\] 和 \[google home\]\[Google\] 等效

# 圖片
圖片的寫法 類似 連接  
只需要在 連接的寫法前加上 ! 即可  
使用 !\[xxx\]\(url "title"\) 或 !\[xxx\]\[id\] 的方式

# 強調
使用 * _ 將 文本包起來 會被轉爲&lt;em&gt;標籤 使用兩個則會被轉爲&lt;strong&gt; 

```text
*single asterisks*

_single underscores_

**double asterisks**

__double underscores__
```
```html
<em>single asterisks</em>

<em>single underscores</em>

<strong>double asterisks</strong>

<strong>double underscores</strong>
```
使用 \ 轉義 可以輸出 普通的 * _
```text
\*cerberus is an idea\*
```
```
*cerberus is an idea*
```
# 程式碼
在行內 使用 \` 將 程式碼包起來 如果程式碼中 也有 \` 則使用 多個 \` 包裹即可
```text
`````` fmt.prinln(`123`) ``````
`fmt.Println(123)`
```